﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExCaixeiroViajante
{
    public class CalcularDistancia
    {
        

        public int Calcular()
        {
            var matriz = new[] { 0, 9, 4, 8, 9, 0, 5, 8, 4, 5, 0, 3, 8, 8, 3, 0 };
            var c = 0;
            var x = 0; 
            var v = new int[6];
            var vc2 = new int[6];
            var vc3 = new int[6];
            var vc4 = new int[6];

            Console.WriteLine("\n   Goiania 1\n   Uberaba 2\n   Uberlandia 3\n   Belo Horizonte 4\n\n");
            Console.WriteLine("Digite o numero a cidade de Origem:  {0}", x);

            if ((x < 1) || (x > 4))
            {
                Console.WriteLine("\n\nCidade errada");
                Console.ReadLine();
            }
            else
            {
                int i;
                var j = i = (x - 1);
                v[c] = matriz[j][i];

                int k;
                for (k = 0; k < 4; k++)
                {
                    if (k != i)
                    {
                        v[c] = v[c] + matriz[j][k];
                        v[c + 1] = v[c];
                        vc2[c] = k + 1;
                        vc2[c + 1] = vc2[c];

                        int m;
                        for (m = 0; m < 4; m++)
                        {
                            int n;
                            for (n = 0; n < 4; n++)
                            {
                                if ((n == k) && (m != j) && (m != k))
                                {
                                    v[c] = v[c] + matriz[m][n];
                                    vc3[c] = m + 1;

                                    int r;
                                    for (r = 0; r < 4; r++)
                                    {
                                        int s;
                                        for (s = 0; s < 4; s++)
                                        {
                                            if ((r == m) && (s != i) && (s != n) && (s != r))
                                            {
                                                v[c] = v[c] + matriz[r][s];
                                                vc4[c] = s + 1;
                                                c++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var o = 0; 
                for (i = 0; i < 6; i++)
                {
                    if (v[i] < v[i + 1])
                    {
                        o = i;
                        if (v[5] < v[i])
                            o = 5;
                    }
                }
                Console.ReadLine();

                for (i = 0; i < 6; i++)
                    Console.WriteLine("[{0}]\n", v[i]);
                Console.WriteLine(
                    "\nSaindo da cidade {0}: o menor caminho seria passando\n pela cidade {1} " +
                    "depois por {2} e por fim em  {3}. \n\nNo total seria {4} Km.\n",
                    x, vc2[o], vc3[o], vc4[o], v[o]);
                Console.ReadLine();
            }
            return (0);
        }
    }
}
